#pragma once

#include "Ogre.h"
#include "OgreApplicationContext.h"
#include "OgreInput.h"
#include "OgreRTShaderSystem.h"
#include "OgreApplicationContext.h"
#include "OgreCameraMan.h"
//#include "BtOgrePG.h"
//#include "BtOgreGP.h"
//#include "BtOgreExtras.h"
/* Bullet3 Physics */
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"
#include "Player.h"
using namespace Ogre;
using namespace OgreBites;

class Game : public ApplicationContext, public InputListener
{
private:
    SceneManager* scnMgr;
    // Current animation state - contains relative timeline.
    AnimationState* mBoxAnimationState;
    /*AnimationState* mNinjaAnimationState;*/
    //// collision configuration.
    btDefaultCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();

    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    btCollisionDispatcher* dispatcher = new btCollisionDispatcher(collisionConfiguration);

    ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
    btBroadphaseInterface* overlappingPairCache = new btDbvtBroadphase();

    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver;
    btDiscreteDynamicsWorld* dynamicsWorld;

    ///keep track of the shapes, we release memory at exit.
    //make sure to re-use collision shapes among rigid bodies whenever possible!
    btAlignedObjectArray<btCollisionShape*> collisionShapes;
    SceneNode* boxNode;
    //Ninja also
    Entity* tankEntity;
    SceneNode* tankSceneNode;

    Entity* barrelEntity;
    SceneNode* barrelSceneNode;
    SceneNode* mCameraPivot;
    SceneNode* mCameraGoal;
    SceneNode* mCameraNode;
    Real mPivotPitch;
    SceneNode* mBodyNode;
    
    Player* player;
    
    /**
    * w key flag - should this be here?
    * This is a questionable design decision, shouldn't such behaviour be delegated.
    */
    bool wDown;

    /**
    * w key flag - should this be here?
    * This is a questionable design decision, shouldn't such behaviour be delegated.
    */
    bool aDown;

    bool dDown;
    bool sDown;
    float cam_height;
   /* BtOgre::DebugDrawer* dbgdraw;*/
    //Debugging flags.
    bool ogreDebugDraw;
    bool bulletDebugDraw;

public:

	Game();
	virtual ~Game();

	void setup();
    void setupCamera(Camera* cam);
	void setupCamera();
    void updateCamera(Real deltaTime);
    void setupTankMesh();

   /* void setupNinjaAnimation();*/
    void setupBoxAnimation();

	void setupBoxMesh();
    void setupNewBoxMesh();
    void setupPlayer();
    void setupFloor();
    void setupBarrelMesh();
	void setupLights();
	bool keyPressed(const KeyboardEvent& evt);
    bool keyReleased(const KeyboardEvent& evt);
	bool mouseMoved(const MouseMotionEvent& evt);

	bool frameStarted (const FrameEvent &evt);
	bool frameEnded(const FrameEvent &evt);
    bool frameRenderingQueued(const FrameEvent& evt);
    void bulletInit();
};
